#!/bin/sh
hostname=""  # pre.domain.tld
password=""  # dyndns key
ip=$(ip a | grep inet6 | grep global | grep -v ' fd' | grep -v ' fe' | cut -d' ' -f6 | cut -d'/' -f1)  # gets the current (not local link) ipv6 address

touch /tmp/old_ip

if [ "$(cat /tmp/old_ip)" != "$ip" ]; then
    curl "https://dyn.dns.he.net/nic/update" -d "hostname=$hostname" -d "password=$password" -d "myip=$ip"
    echo -n $ip > /tmp/old_ip
fi
