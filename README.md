# [Hurricane Electric Free DNS](https://dns.he.net/) DynDNS Systemd timer
## Setup
- Copy `dyndns.*` to `/etc/systemd/system/`
- Set the variables in `dyndns.sh`
- `systemctl daemon-reload`
- `systemctl enable dyndns.timer`
- `systemctl start dyndns.timer`
